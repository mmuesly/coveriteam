#!/usr/bin/make -f

# This file is part of CoVeriTeam, a tool for on-demand composition of cooperative verification systems:
# https://gitlab.com/sosy-lab/software/coveriteam
#
# SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
#
# SPDX-License-Identifier: Apache-2.0

# -*- makefile -*-

# Uncomment this to turn on verbose mode.
# export DH_VERBOSE=1

export PYBUILD_NAME=coveriteam

# Our tests need some additional files in {build_dir} to execute succesfully
export PYBUILD_BEFORE_TEST=cp -r {dir}/bin {build_dir}/
export PYBUILD_AFTER_TEST=rm -r {build_dir}/bin

%:
	dh $@ --with python3 --buildsystem=pybuild

override_dh_auto_build:
	dh_auto_build
	python3 setup.py build

override_dh_auto_install:
	dh_auto_install
	python3 setup.py install --root=$(CURDIR)/debian/$(DEB_SOURCE) --install-layout=deb

# override dh_auto_test to prevent its execution
override_dh_auto_test:

override_dh_installchangelogs:
	dh_installchangelogs CHANGELOG.md

override_dh_auto_clean:
	dh_auto_clean
	rm -rf build
	rm -rf *.egg-info
