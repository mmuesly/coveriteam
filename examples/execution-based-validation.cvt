// This file is part of CoVeriTeam, a tool for on-demand composition of cooperative verification systems:
// https://gitlab.com/sosy-lab/software/coveriteam
//
// SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

// Execution-Based Validation

w2test = ActorFactory.create(WitnessToTest, "../actors/cpachecker-witness-to-test.yml");
testval = ActorFactory.create(TestValidator, "../actors/test-val.yml");
spec_to_testspec = SpecToTestSpec();
execution_based_validator = SEQUENCE(PARALLEL(w2test, spec_to_testspec), testval);

// Prepare test inputs.
witness = ArtifactFactory.create(ReachabilityWitness, witness_path);
prog = ArtifactFactory.create(CProgram, prog_path);
spec = ArtifactFactory.create(BehaviorSpecification, spec_path);
ip = {'program':prog, 'spec':spec, 'witness':witness};

// Execute the actor on the inputs.
execute(execution_based_validator, ip);

